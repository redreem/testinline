<?php

require_once ENVIROMENT_CONFIG_DIR . 'db.config.php';

require_once ENVIROMENT_CORE_DIR . 'MyLogger.php';
require_once ENVIROMENT_CORE_DIR . 'PDOAdapter.php';

require_once ENVIROMENT_APPLICATION_DIR . 'Application.php';

class Core {

    public static $app;
    public static $config = [
        'core' => [],
        'db' => [],
        'application' => [],
    ];

    /**
     * @var $pdo PDOAdapter
     */

    public static $pdo;
    public static $errorLogger;
    public static $logFileName;

    public static final function execute()
    {
        self::setCoreConfig();
        self::setDbConfig();
        self::setApplicationConfig();

        if (self::$config['core']['debug']) {

            ini_set('error_reporting', E_ALL);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);

        } else {

            ini_set('error_reporting', 0);
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
        }

        self::setLogFile();
        self::$errorLogger = new MyLogger(self::$logFileName);

        $dsn = "mysql:host=localhost;dbname=" . self::$config['db']['base'];

        self::$pdo = new PDOAdapter(
            $dsn,
            self::$config['db']['user'],
            self::$config['db']['password'],
            self::$errorLogger
        );

        self::$app = new Application();
        self::$app->execute();
    }

    private static final function setLogFile()
    {
        self::$logFileName = ENVIROMENT_LOG_DIR . 'log.txt';
    }

    private static final function setCoreConfig()
    {
        self::$config['core'] = include ENVIROMENT_CONFIG_DIR . 'core.config.php';
    }

    private static final function setDbConfig()
    {
       self::$config['db'] = include ENVIROMENT_CONFIG_DIR . 'db.config.php';
    }

    private static final function setApplicationConfig()
    {
       self::$config['application'] = include ENVIROMENT_CONFIG_DIR . 'application.config.php';
    }
}