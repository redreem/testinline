<!DOCTYPE html>
<head>
    <link rel="stylesheet" title="default" href="public/css/style.css" type="text/css"/>
</head>
<body>

    <div id="main_wrap">

        <h1>Список персон максимального возраста (<?php echo $this->app_data['max_age']; ?>)</h1>

        <div class="wrap_cols">

            <div class="col_3_1 table_head">Фамилия</div>
            <div class="col_3_2 table_head">Имя</div>
            <div class="col_3_3 table_head">Возраст</div>

            <?php foreach ($this->app_data['max_age_persons'] as $person) {?>

            <div class="col_3_1"><?php echo $person[0]; ?></div>
            <div class="col_3_2"><?php echo $person[1]; ?></div>
            <div class="col_3_3"><?php echo $person[2]; ?></div>

            <?php } ?>

            <div class="clear"></div>

        </div>

    </div>

</body>