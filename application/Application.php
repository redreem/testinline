<?php

require_once ENVIROMENT_APPLICATION_DIR . 'helper' . DIRECTORY_SEPARATOR . 'SqlHelper.php';

class Application {

    public $app_data = [

        'max_age' => 0,
        'one_person_by_conditions' => [],
        'max_age_persons' => [],
    ];

    /**
    * Экземпляр PDOAdapter, описан тут для работоспособности автоподстановки
    * @var Core::$pdo PDOAdapter
    */

    /**
    * Основное выполнение
    */
    public function execute()
    {
        Core::$pdo->execute('execute', SQLHelper::getSql('DropPersonsTable'));
        $this->db_init();

        // -- этот блок нужен был для тестирования
        // -- проверить существование таблицы person, если нет, - создать и заполнить
        //if (!$this->checkTableExists('person')) {
            //$this->db_init();
        //}

        // -- 1) Определить максимальный возраст
        $data = Core::$pdo->execute(
            'selectOne',
            SQLHelper::getSql('MaxAge')
        );
        $this->app_data['max_age'] = $data->maxage;

        // -- 2) Найти любую персону, у которой mother_id не задан и возраст меньше максимального
        $data = Core::$pdo->execute(
            'selectAll',
            SQLHelper::getSql('OnePersonByConditions'),
            [
                ':maxage' => $this->app_data['max_age'],
            ]
        );

        $this->app_data['one_person_by_conditions'] = [

            'id'        => $data[0]->id,
            'mother_id' => $data[0]->mother_id,
            'spouse_id' => $data[0]->spouse_id,
            'lastname'  => $data[0]->lastname,
            'firstname' => $data[0]->firstname,
            'age'       => $data[0]->age,
        ];

        // -- 3) изменить у нее возраст на максимальный
        Core::$pdo->execute(
            'execute',
            SQLHelper::getSql('UpdateOnePersonByConditionsAge'),
            [
                ':id' => $this->app_data['one_person_by_conditions']['id'],
                ':age' => $this->app_data['max_age'],
            ]
        );

        // -- 4) Получить список персон максимального возраста (фамилия, имя). НЕ ИСПОЛЬЗУЯ полученное на шаге 1 значение
        $data = Core::$pdo->execute(
            'selectAll',
            SQLHelper::getSql('MaxAgePersons'),
            []
        );

        foreach ($data as $stdObj) {

            array_push(
                $this->app_data['max_age_persons'],
                [
                    $stdObj->lastname,
                    $stdObj->firstname,
                    $stdObj->age,
                ]
            );
        }

        echo include ENVIROMENT_APPLICATION_DIR . 'template' . DIRECTORY_SEPARATOR . 'ApplicationTemplate.php';
    }

    /**
     * @param $table_name
     * @return bool
     */
    private function checkTableExists($table_name)
    {

        $data = Core::$pdo->execute(
            'selectAll',
            SQLHelper::getSql('ShowTable'),
            [
                ':tablename' => $table_name
            ]
        );

        return (count($data) == 1 ? true : false);
    }

    /**
     * инициализация данных в базе
     */
    private function db_init()
    {
        Core::$pdo->execute('execute', SQLHelper::getSql('CreatePersonsTable'));
        Core::$pdo->execute('execute', SQLHelper::getSql('InsPersonsTable'));
    }
}