<?php

class SQLHelper {

    public static final function getSql($sql_name)
    {

        $sql = '';

        switch ($sql_name) {

            case 'MaxAge':
                $sql .= "
                    select 
                        max(p.age) maxage 
                    from person p 
                ";
                break;

            case 'OnePersonByConditions':
                $sql = "
                    select 
                      * 
                    from person p 
                    where 
                      p.mother_id is null
                      and 
                      p.age < :maxage 
                    limit 1
                ";
                break;

            case 'UpdateOnePersonByConditionsAge':
                $sql = "
                    update person p 
                    set 
                        p.age = :age
                    where 
                       id = :id
                ";
                break;

            case 'MaxAgePersons':
                $sql = "
                    select 
                        p.lastname,
                        p.firstname,
                        p.age 
                    from person p 
                    where 
                        age = (select max(age) from person)
                    order by
                        p.lastname,
                        p.firstname
                ";
                break;

            case 'DeletePersonsTable':
                $sql = "
                    delete person
                ";
                break;

            case 'CreatePersonsTable':
                $sql = "
                    CREATE TABLE `person` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `mother_id` int(11) DEFAULT NULL COMMENT 'id матери	',
                        `spouse_id` int(11) DEFAULT NULL COMMENT 'id супруга(супруги)',
                        `lastname` varchar(64) NOT NULL,
                        `firstname` varchar(64) NOT NULL,
                        `age` smallint(6) NOT NULL,
                    PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8
                ";
                break;

            case 'InsPersonsTable':
                $sql = "
                    INSERT INTO `person` VALUES 
                    (1,NULL,4,'Иванов','Иван',40),
                    (2,4,NULL,'Иванов','Вася',4),
                    (3,4,NULL,'Иванова','Даша',6),
                    (4,NULL,1,'Иванова','Мария',32),
                    (5,NULL,NULL,'Петров','Петр',46),
                    (6,NULL,8,'Сидоров','Сандро',46),
                    (7,NULL,NULL,'Сидорова','Елена',40),
                    (8,NULL,6,'Волкова','Ксения',39),
                    (9,8,NULL,'Волкова','Настя',9)
                ";
                break;

            case 'ShowTable':
                $sql = "
                    show tables like :tablename
                ";
                break;

            case 'DropPersonsTable':
                $sql = "drop table `person`";
                break;
        }

        return $sql;
    }
}